<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,inital-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<title>登陆页面</title>
<link href="${pageContext.request.contextPath}/plugins/css/login.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/js/jquery.validate.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery("#theForm").validate({
				errorPlacement : function(error, element) {
					element.parent().addClass("bd_red");
					error.appendTo(element.parent().next());
				},
				success : function(label) {
					label.parent().prev().removeClass("bd_red");
				},
				rules : {
					username : {
						required : true
					},
					password : {
						required : true
					},
					code : {
						required : true,
						remote : {
							url : "${pageContext.request.contextPath}/verify_code.htm", //后台处理程序
							type : "post", //数据发送方式
							dataType : "json", //接受数据格式   
							data : { //要传递的数据
								"code" : function() {
									return jQuery("#code").val();
								}
							}
						}
					}
				},
				messages : {
					username : {
						required : "用户名不能为空"
					},
					password : {
						required : "密码不能为空"
					},
					code : {
						required : "验证码不能为空",
						remote : "验证码不正确"
					}
				}
			});
		});
	</script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
	 var url = window.location.href;
	   var timestamp;
	   var noncestr;
	   var signature;
	   var appId;
	   //获取签名
	jQuery.ajax({
		url: "${pageContext.request.contextPath}/wxcfigServlet?method=getJssdkSignature",
		type: "POST",  
		dataType : "json",
		async: false,
		data:{url:url},
		cache:true,  
		success: function(data) {
			 timestamp=data.timestamp; 
		        noncestr=data.noncestr; 
		        signature=data.signature;
		        appId=data.appId;
		         wxShare();
		},
		error: function() {
		}
	}); 
function wxShare(){
 wx.config({
    debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId:appId, // 必填，公众号的唯一标识
    timestamp:timestamp , // 必填，生成签名的时间戳
    nonceStr:noncestr, // 必填，生成签名的随机串
    signature:signature,// 必填，签名，见附录1
    jsApiList:[ 'onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQZone','onMenuShareQQ','onMenuShareWeibo']
}); 
}
wx.ready(function () {
	  // 在这里调用 API
	   wx.onMenuShareTimeline({
	      title: '【微信网页OAuth2.0授权之微信jssdk分享-程序媛】', // 分享标题
	      desc:"这个微信网页授权栗子我是在百忙之中写的 有不足之处还望多多谅解!嘻嘻",
	      link: 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx292e1dfb5c3648d4&redirect_uri=http%3A%2F%2F13786147292.tunnel.2bdata.com%2FWeixinAuthorizeServlet%2FwxUserServlet%3Fmethod%3DloginPage&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect', // 分享链接
	      imgUrl: 'http://13786147292.tunnel.2bdata.com${pageContext.request.contextPath}/plugins/image/head.jpg', // 分享图标
	      success: function () { 
	          alert('分享到朋友圈成功！');
	          // 用户确认分享后执行的回调函数
	      },
	      cancel: function () { 
	          // 用户取消分享后执行的回调函数
	      }
	  }); 
	  wx.onMenuShareAppMessage({
		  title: '【微信网页OAuth2.0授权之微信jssdk分享-程序媛】', // 分享标题
	      desc:"这个微信网页授权栗子我是在百忙之中写的 有不足之处还望多多谅解!嘻嘻",
	      link: ' http://13786147292.tunnel.2bdata.com/WeixinAuthorizeServlet/wxUserServlet?method=loginPage', // 分享链接
	      imgUrl: 'http://13786147292.tunnel.2bdata.com${pageContext.request.contextPath}/plugins/image/head.jpg', // 分享图标
	     	type: '', // 分享类型,music、video或link，不填默认为link
	      dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
	      success: function () { 
	          alert('分享给微信朋友成功!');
	          // 用户确认分享后执行的回调函数
	      },
	      cancel: function () { 
	          // 用户取消分享后执行的回调函数
	      }
	  });
	  
	  wx.onMenuShareQZone({//分享到qq空间
		  title: '【微信网页OAuth2.0授权之微信jssdk分享-程序媛】', // 分享标题
	      desc:"这个微信网页授权栗子我是在百忙之中写的 有不足之处还望多多谅解!嘻嘻",
	      link: 'http://1805b684.nat123.net/zlkjcms/xuexiao/162.html', // 分享链接
	      imgUrl: 'http://13786147292.tunnel.2bdata.com${pageContext.request.contextPath}/plugins/image/head.jpg', // 分享图标
		    success: function () { 
		    	 alert('分享成功!');
		       // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
	  wx.onMenuShareQQ({//分享到qq空间
		  title: '【微信网页OAuth2.0授权之微信jssdk分享-程序媛】', // 分享标题
	      desc:"这个微信网页授权栗子我是在百忙之中写的 有不足之处还望多多谅解!嘻嘻",
	      link: 'http://1805b684.nat123.net/zlkjcms/xuexiao/162.html', // 分享链接
	      imgUrl: 'http://13786147292.tunnel.2bdata.com${pageContext.request.contextPath}/plugins/image/head.jpg', // 分享图标
		    success: function () { 
		    	 alert('分享成功!');
		       // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		       // 用户取消分享后执行的回调函数
		    }
		});
	  wx.onMenuShareWeibo({//分享到腾讯微博
		  title: '【微信网页OAuth2.0授权之微信jssdk分享-程序媛】', // 分享标题
	      desc:"这个微信网页授权栗子我是在百忙之中写的 有不足之处还望多多谅解!嘻嘻",
	      link: 'http://1805b684.nat123.net/zlkjcms/xuexiao/162.html', // 分享链接
	      imgUrl: 'http://13786147292.tunnel.2bdata.com${pageContext.request.contextPath}/plugins/image/head.jpg', // 分享图标
		    success: function () { 
		    	 alert('分享成功!');
		       // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
	  
	});
	
	
wx.error(function(res){
	alert("验证失败！");
    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
});
</script>
	
</head>
<body>
	<div class="phone_hd">
		<a class="back" href="javascript:history.go(-1);">
			<img src="${pageContext.request.contextPath}/plugins/image/back.png" width="25" height="25">
		</a>登录<a class="menu home"
			href="${pageContext.request.contextPath}/wap/index.htm"><img src="${pageContext.request.contextPath}/plugins/image/home.png" width="25" height="25"></a>
	</div>
	<div class="phone_main">
		<!--登录页-->
		<form action="#"
			method="post" name="theForm" id="theForm" novalidate="novalidate">
			<input name="login_role" type="hidden" id="login_role" value="user">
			<input name="wemall_view_type" type="hidden" id="wemall_view_type" value="wap">
				<div class="phone_login">
					<ul>
						<li class="ip"><input name="username" id="username" type="text" placeholder="请输入用户名/邮箱/已验证手机"></li>
						<li class="yz"></li>
						<li class="ip"><input name="password" id="password" type="password" placeholder="请输入密码"></li>
						<li class="yz"></li>
						<script>
							function refreshCode() {
								jQuery("#code_img").attr("src", "${pageContext.request.contextPath}/verify.htm?d" + new Date().getTime());
							}
						</script>
						<li class="yzm">
							<input name="code" type="text" id="code" placeholder="请输入验证码"> 
								<img id="code_img" src="" width="73" height="27">
								<a href="javascript:void(0);" onclick="refreshCode();">
								<img src="${pageContext.request.contextPath}/plugins/image/refresh.png"
										width="32" height="32"></a></li>
						<li class="yz"></li>
						<li class="ip_btn"><input type="submit" value="登录"></li>
					</ul>
				</div>
		</form>
	</div>
</body>
</html>