package com.zlkj.weixin.entity;
import java.io.Serializable;
/**
 * JS-SDK Ticket 对象
 * @author 程序媛
 * QQ群 群1： 494705674 群2:605806884
 */
public class JSTicket implements Serializable{
	private static final long serialVersionUID = 1L;
	private String errcode;
	private String errmsg;
	private String ticket;
	private String expires_in;
	public String getErrcode() {
		return errcode;
	}
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}
}
