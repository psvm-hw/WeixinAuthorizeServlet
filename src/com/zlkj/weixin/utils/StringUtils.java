package com.zlkj.weixin.utils;

import java.util.UUID;

public class StringUtils {
	public static String getUUID()throws Exception{
		return UUID.randomUUID().toString().replace("-", "").trim();
	}
}
