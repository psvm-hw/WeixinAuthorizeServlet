package com.zlkj.weixin.web.servlet;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import static com.zlkj.weixin.base.Base.notEmpty;
import static com.zlkj.weixin.utils.ResponseUtils.renderJson;
import static com.zlkj.weixin.base.Base.empty;

import com.zlkj.weixin.cfg.WeiXinConfig;
import com.zlkj.weixin.dto.TicketSignDTO;
import com.zlkj.weixin.dto.WeiXinConfigSignatureDTO;

import static com.zlkj.weixin.kit.WeiXinEventProcessinKit.weiXinEventProcessin;
import static com.zlkj.weixin.kit.WeiXinSecurityKit.jssdkSignature;

import com.zlkj.weixin.kit.WeiXinMassageKit;
import com.zlkj.weixin.kit.WeiXinSecurityKit;
import com.zlkj.weixin.utils.CalendarUtil;
import com.zlkj.weixin.utils.JsonUtil;
import com.zlkj.weixin.utils.WeiXinFinalValue;

import static com.zlkj.weixin.utils.RequestUtil.toBean;

/**
 * 微信Servlet web层
 * @author 程序媛
 * QQ群 群1： 494705674 群2:605806884
 */
public class WeiXinConfigurerServlet extends BaseServlet{
	private static final long serialVersionUID = 1L;
	/**
	 * 微信配置入口
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * http://13786147292.tunnel.2bdata.com/WeixinAuthorizeServlet/wxcfigServlet?method=wxCfgEntrance
	 */
	public void wxCfgEntrance(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			WeiXinConfigSignatureDTO wcfg
			 		=toBean(request.getParameterMap(), WeiXinConfigSignatureDTO.class);
			String[] arrys = {WeiXinFinalValue.TOKEN,wcfg.getTimestamp(), wcfg.getNonce()};
			Arrays.sort(arrys);
			StringBuffer buffer=new StringBuffer();
			for(String arr:arrys){
				buffer.append(arr);
			}
			String shalMsg=WeiXinSecurityKit.shal(buffer.toString());
			if (notEmpty(wcfg.getSignature()) && wcfg.getSignature().equals(shalMsg)&&notEmpty(wcfg.getEchostr())) {
				renderJson(response, wcfg.getEchostr());
				return;
			}
			  String encryptType = empty(wcfg.getEncrypt_type()) ?"raw" :wcfg.getEncrypt_type();
				Map<String, Object> messageMap=null;
				String responseMessage =null;
				    if ("raw".equals(encryptType)) {// 明文传输的消息
				    	System.out.println("【明文传输的消息】------------>>");
				    	 messageMap = WeiXinMassageKit.handlerProcessProclaimedMessage(request);
				    	}else if ("aes".equals(encryptType)) {
				    		System.out.println("【是aes加密传输的消息】------------>>");
				    	 messageMap = WeiXinMassageKit.handlerProcessEncryptionMessage(request,wcfg);
				    }
					 responseMessage =weiXinEventProcessin(messageMap);// 响应消息
					System.out.println("------------->>>>" + responseMessage);
					response.setContentType("application/xml;charset=UTF-8");
					response.setCharacterEncoding("UTF-8");
					if (notEmpty(responseMessage)) 	// 响应给微信公众号
						response.getWriter().write(responseMessage);// 回复微信用户 被动回复
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 微信JS接口验证入口分享签名
	 * TODO 吧生成的jsapi_ticket保存到缓存或者单列的Bean属性中 
	 * 调用js接口的凭证jsapi_ticket 如果存在 判断是否超时 有效时间为 7200秒
	 * 如果超时重新获取
	 * 请求url:http://13786147292.tunnel.2bdata.com/WeixinAuthorizeServlet/wxcfigServlet?method=getJssdkSignature
	 */
	public void getJssdkSignature(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			String ReqUrl = request.getParameter("url");
			TicketSignDTO ticketSignDTO = new TicketSignDTO();
			ticketSignDTO.setUrl(ReqUrl);
			ticketSignDTO.setTimestamp(String.valueOf(CalendarUtil.systemSeconds()));
			ticketSignDTO.setNoncestr(com.zlkj.weixin.utils.StringUtils.getUUID());
			ticketSignDTO.setAppId(
					WeiXinFinalValue.APPID);
			ticketSignDTO.setSignature(
					jssdkSignature(notEmpty(WeiXinConfig.getInstance().getjSTicket())?
							WeiXinConfig.getInstance().getjSTicket().getTicket():null,
								ticketSignDTO.getTimestamp(),ticketSignDTO.getNoncestr(), ReqUrl));
			renderJson(response, JsonUtil.getInstance().obj2json(ticketSignDTO));
	}
	public static void main(String[] args) {
		System.out.println(Integer.valueOf((int) 1.0*100).intValue());
		try {
			System.out.println(URLDecoder.decode("firstname=%E5%BC%A0%E4%B8%89", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
