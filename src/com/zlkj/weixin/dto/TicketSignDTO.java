package com.zlkj.weixin.dto;
import java.io.Serializable;
/**
 * 微信Js接口签名DTO
 * @author 程序媛
 * QQ群 群1： 494705674 群2:605806884
 */
public class TicketSignDTO implements Serializable{
private static final long serialVersionUID = 1L;
private String timestamp;
private String signature;
private String appId;
private String url;
private String noncestr;
public String getTimestamp() {
	return timestamp;
}
public void setTimestamp(String timestamp) {
	this.timestamp = timestamp;
}
public String getSignature() {
	return signature;
}
public void setSignature(String signature) {
	this.signature = signature;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getNoncestr() {
	return noncestr;
}
public void setNoncestr(String noncestr) {
	this.noncestr = noncestr;
}
public String getAppId() {
	return appId;
}
public void setAppId(String appId) {
	this.appId = appId;
}
}
